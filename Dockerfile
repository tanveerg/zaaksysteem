FROM python:3.6

ENV PYTHONUNBUFFERED 0

RUN mkdir /zaaksysteem && mkdir /zaaksysteem/logs

WORKDIR /zaaksysteem

ADD . /zaaksysteem/

RUN pip install -r requirements.txt && python setup.py develop
