from pyramid.config import Configurator


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings)
    config.include('pyramid_jinja2')
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('swagger', 'swagger')
    config.add_route('spec', 'spec')
    config.add_route('singleop', 'singleop/{operation}/{operand1}/{operand2}/')
    config.add_route('complex', 'complex/')
    config.scan()
    return config.make_wsgi_app()
