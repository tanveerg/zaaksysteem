import unittest

from pyramid import testing


class ViewTests(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()

    def test_my_view(self):
        from .views import my_view
        request = testing.DummyRequest()
        info = my_view(request)
        self.assertEqual(info['project'], 'zaaksysteem')


class FunctionalTests(unittest.TestCase):
    def setUp(self):
        from zaaksysteem import main
        app = main({})
        from webtest import TestApp
        self.testapp = TestApp(app)

    def test_root(self):
        res = self.testapp.get('/', status=200)
        self.assertTrue(b'Pyramid' in res.body)


class CalculatorTests(FunctionalTests):
    """Test suite for the calculator endpoints"""
    def test_swagger_endpoint(self):
        """Ensure you can load the swagger doc"""
        response = self.testapp.get('/swagger')
        self.assertEqual(response.status_code, 200)

    def test_api_spec_endpoint(self):
        """Ensure you can load the api spec"""
        response = self.testapp.get('/spec')
        self.assertEqual(response.status_code, 200)

    def test_singleop_endpoint_success(self):
        """Sample successful result of the singleop endpoint"""
        response = self.testapp.get('/singleop/add/1/2/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.body, b'{"value": 3.0}')

    def test_singleop_endpoint_invalid_operation(self):
        """Test with supplying invalid operation"""
        response = self.testapp.get('/singleop/ad/1/2/', expect_errors=True)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.body,
            b'{"errors": "invalid operation supplied"}')

    def test_singleop_endpoint_invalid_operands(self):
        """Test with supplying invalid operand"""
        response = self.testapp.get('/singleop/add/1a/2/', expect_errors=True)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.body,
            b'{"errors": "one or both of the operands are invalid"}')

    def test_singleop_endpoint_zero_division(self):
        """Test whether 0 division throws an error or not"""
        response = self.testapp.get('/singleop/div/1/0/', expect_errors=True)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.body,
            b'{"errors": "second operand cannot be 0 for division operation"}')

    def test_complex_endpoint_no_expression(self):
        """Test 400 on no expression supplied in complex endpoint"""
        response = self.testapp.get('/complex/', expect_errors=True)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.body,
            b'{"errors": "no expression found to be evaluated"}')

    def test_complex_endpoint_success(self):
        """Test valid complex expression request"""
        response = self.testapp.get('/complex/',
                                    {'expression': '23 * 453446 / 66'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.body,
            b'{"value": 158019.0606060606}')

    def test_complex_endpoint_error(self):
        """Test 400 on erroneous expression supplied"""
        response = self.testapp.get('/complex/',
                                    {'expression': '23 * 453446 / 0'},
                                    expect_errors=True)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.body,
            b'{"errors": "Expression resulted in an error"}')

    def test_complex_endpoint_ignored(self):
        """Test 400 on ignored characters in expression"""
        response = self.testapp.get('/complex/',
                                    {'expression': '23 abc 453446  0'},
                                    expect_errors=True)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.body,
            b'{"errors": "Something was ignored during the evaluation of the expression"}')

    def test_complex_endpoint_invalid_expression(self):
        """Test 400 when expression supplied could not be evaluated"""
        response = self.testapp.get('/complex/',
                                    {'expression': ''},
                                    expect_errors=True)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.body,
            b'{"errors": "Expression could not be evaluated"}')
