import json
import logging
import os

from calculator import simple
from pyramid import response
from pyramid.view import view_config


INVALID_OPERANDS_ERROR_DICT = {
    'errors': 'one or both of the operands are invalid'
}

INVALID_OPERATION_ERROR_DICT = {
    'errors': 'invalid operation supplied'
}

ZERO_DIVISION_ERROR_DICT = {
    'errors': 'second operand cannot be 0 for division operation'
}

MISSING_EXPRESSION_ERROR_DICT = {
    'errors': 'no expression found to be evaluated'
}

CWD_SCRIPT = os.path.dirname(os.path.realpath(__file__))
"""Directory of this script"""

LOGGER = logging.getLogger(__name__)


class InvalidOperationException(ValueError):
    """Raised when the single operation supplied is invalid"""
    pass


class InvalidExpressionException(ValueError):
    """Raised when the passed in expression is invalid"""
    def __init__(self, message):
        self.message = message


@view_config(route_name='home', renderer='templates/mytemplate.jinja2')
def my_view(request):
    """Default home page"""
    return {'project': 'zaaksysteem'}


@view_config(route_name='swagger', renderer='json')
def swagger(request):
    """Swagger doc endpoint

    :param request: pyramid.request object

    :rtype: pyramid.response.Response object
    """
    with open('{}/templates/swagger.json'.format(
            CWD_SCRIPT)) as swagger_file:
        deserialized = json.load(swagger_file)
        return response.Response(
            content_type='application/json; charset=UTF-8',
            body=json.dumps(deserialized))


@view_config(route_name='spec', renderer='templates/spec.jinja2')
def spec(request):
    """API spec endpoint

    :param request: pyramid.request object

    :rtype: pyramid.response.Response object
    """
    return {}


class BaseEndpointClass:
    """Base class for calculation endpoints"""
    def __init__(self, request):
        self.request = request


class SingleOperationEndpoint(BaseEndpointClass):
    """Class for single operation endpoint"""
    def __init__(self, request):
        super().__init__(request)
        self.operation = self.request.matchdict['operation']
        self.operand_1 = self.request.matchdict['operand1']
        self.operand_2 = self.request.matchdict['operand2']

    def _evaluate(self):
        """Evaluates the single operation

        :rtype: float
        """
        float_operand_1 = float(self.operand_1)
        float_operand_2 = float(self.operand_2)

        if self.operation == 'add':
            return float_operand_1 + float_operand_2
        elif self.operation == 'sub':
            return float_operand_1 - float_operand_2
        elif self.operation == 'multi':
            return float_operand_1 * float_operand_2
        elif self.operation == 'div':
            return float_operand_1 / float_operand_2
        elif self.operation == 'pow':
            return float_operand_1 ** float_operand_2
        else:
            raise InvalidOperationException

    @view_config(route_name='singleop', renderer='json')
    def single_operation_endpoint(self):
        """Single operation endpoint

        :rtype: pyramid.response.Response object
        """
        try:
            evaluation = self._evaluate()
            if isinstance(evaluation, complex):
                LOGGER.debug('Evaluation resulted in complex expression')
                evaluation = '{} + {}j'.format(
                    evaluation.real, evaluation.imag)
            return response.Response(
                content_type='application/json; charset=UTF-8',
                body=json.dumps({'value': evaluation})
            )
        except ZeroDivisionError:
            LOGGER.debug('Evaluation resulted in divide by 0')
            self.request.response.status = 400
            return ZERO_DIVISION_ERROR_DICT
        except InvalidOperationException:
            LOGGER.error('Invalid operation supplied - "%s"',
                         self.operation)
            self.request.response.status = 400
            return INVALID_OPERATION_ERROR_DICT
        except ValueError:
            LOGGER.debug('Invalid operands provided')
            self.request.response.status = 400
            return INVALID_OPERANDS_ERROR_DICT


class ComplexOperationEndpoint(BaseEndpointClass):
    """Class for complex expression operation endpoint"""
    def __init__(self, request):
        super().__init__(request)
        self.calculator = simple.SimpleCalculator()
        self.expression = self.request.params.get('expression')

    @staticmethod
    def _check_ignored(log_list):
        """Checks whether something was ignored during
        expression evaluation

        :param log_list: List

        :rtype: Boolean
        """
        for log_msg in log_list:
            if log_msg.startswith('ignored:'):
                return True
        return False

    def _evaluate_expression(self):
        """Evaluates the complex expression

        :rtype: float
        """
        self.calculator.clear()
        self.calculator.run(self.expression)
        if self.calculator.lcd == 'Error':
            LOGGER.error('Expression "%s" resulted in an error',
                         self.expression)
            raise InvalidExpressionException(
                message='Expression resulted in an error')
        if self._check_ignored(self.calculator.log):
            LOGGER.debug('expression "%s" contains ignored strings',
                         self.expression)
            raise InvalidExpressionException(
                message='Something was ignored during the'
                        ' evaluation of the expression')
        if isinstance(self.calculator.lcd, int) and self.calculator.lcd == 0:
            LOGGER.debug('Expression "%s" could not be evaluated',
                         self.expression)
            raise InvalidExpressionException(
                message='Expression could not be evaluated')
        return self.calculator.lcd

    @view_config(route_name='complex', renderer='json')
    def expression_evaluation_endpoint(self):
        """Complex expression operation endpoint

        :rtype: pyramid.response.Response object
        """
        if self.expression is None:
            LOGGER.error('Missing expression query parameter')
            self.request.response.status = 400
            return MISSING_EXPRESSION_ERROR_DICT
        try:
            evaluation = self._evaluate_expression()
            return response.Response(
                content_type='application/json; charset=UTF-8',
                body=json.dumps({'value': evaluation})
            )
        except InvalidExpressionException as err:
            self.request.response.status = 400
            return {'errors': err.message}
