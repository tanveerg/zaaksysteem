import os

from paste import deploy
import waitress

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app = deploy.loadapp('config:production.ini', relative_to='.')

    waitress.serve(app, host='0.0.0.0', port=port)
