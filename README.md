# Simple Calculator API
Simple [Pyramid](https://trypyramid.com/) micro-service for doing simple (single operation) or complex (expressions) calculations.

It is currently deployed on Heroku and the index page can be accessed via this URL - https://fast-badlands-28105.herokuapp.com/ (loads the starter scaffold page from Pyramid).

## API specification
You can find the OpenAPI/Swagger Specification in `https://fast-badlands-28105.herokuapp.com/spec`
(swagger doc rendered by [redoc.js](https://github.com/Rebilly/ReDoc))

e.g. [https://fast-badlands-28105.herokuapp.com/spec](https://fast-badlands-28105.herokuapp.com/spec)

If you actaully want to see the swagger doc (in JSON format), then go here:

e.g. [https://fast-badlands-28105.herokuapp.com/swagger](https://fast-badlands-28105.herokuapp.com/swagger)

NOTE - If you want to test these in a locally running app then the base URL will be http://localhost:6543/. And the rest would be the same as above.

## Request examples

### Simple Operation Endpoint
This endpoint lets you do simple calculations (addition, subtraction, multiplication, division, exponents/powers) with two operands. It takes in the operation and the 
two operands as dynamic routes - /singleop/{operation}/{operand1}/{operand2}/

 - Addition example: [https://fast-badlands-28105.herokuapp.com/singleop/add/1/2/](https://fast-badlands-28105.herokuapp.com/singleop/add/1/2/)

 - Subtraction example: [https://fast-badlands-28105.herokuapp.com/singleop/sub/1/2/](https://fast-badlands-28105.herokuapp.com/singleop/sub/1/2/)

 - Multiplication example: [https://fast-badlands-28105.herokuapp.com/singleop/multi/1/2/](https://fast-badlands-28105.herokuapp.com/singleop/multi/1/2/)

 - Division example: [https://fast-badlands-28105.herokuapp.com/singleop/div/1/2/](https://fast-badlands-28105.herokuapp.com/singleop/div/1/2/)
 
 - Exponent/power example: [https://fast-badlands-28105.herokuapp.com/singleop/pow/2/3/](https://fast-badlands-28105.herokuapp.com/singleop/pow/2/3/)

 If you prefer the command line (try [curl](https://curl.haxx.se/) or [httpie](https://httpie.org/)); showing `curl` in example
 ```bash
# single operation addition
curl https://fast-badlands-28105.herokuapp.com/singleop/add/1/2/
# single operation division
curl https://fast-badlands-28105.herokuapp.com/singleop/div/1/2/
```

### Complex Operation Endpoint
This endpoint lets you evaluate complex mathematical expressions. It does so by utilizing this [SimpleCalculator](https://github.com/badmetacoder/calculator) python library. 
It takes in the mathematical expression (in string format) as a query parameter. As long as it adheres to the rules of evaluation of the aforementioned mentioned SimpleCalculator library, 
you will get the desired result. If it does not, you will get a 400 response with an error message. Examples of typical 400 responses are shown in the API spec and also in the swagger doc.

 - Sample expression - (1 + 2) [https://fast-badlands-28105.herokuapp.com/complex/?expression=1%20%2B%202](https://fast-badlands-28105.herokuapp.com/complex/?expression=1%20%2B%202)

 - Sample expression - (1 + 2 * 54 fmod 5) [https://fast-badlands-28105.herokuapp.com/complex/?expression=1%20%2B%202%20%2A%2054%20fmod%205](https://fast-badlands-28105.herokuapp.com/complex/?expression=1%20%2B%202%20%2A%2054%20fmod%205)

 If you prefer the command line (try [curl](https://curl.haxx.se/) or [httpie](https://httpie.org/)); showing `curl` in example
 ```bash
# Sample expression 1 + 2
curl -G https://fast-badlands-28105.herokuapp.com/complex/ --data-urlencode "expression=1 + 2"
# Sample expression 1 + 2 * 54 fmod 5
curl -G https://fast-badlands-28105.herokuapp.com/complex/ --data-urlencode "expression=1 + 2 * 54 fmod 5"
```

## Running the app locally
You have two options - already continerized application on Docker Hub or Building from source

### From Docker Hub
* There is an application container on the docker hub, so if you don't want to build from source,
then you can just pull the container from there and then run the app locally
* You would need to have docker for mac installed
* Run the following commands
```bash
# pull the image
docker pull tanveerg/zaaksysteem:latest
# run the application 
docker run -p 6543:6543 --name=zaaksysteem tanveerg/zaaksysteem:latest pserve development.ini --reload
```
* You can verify that the API is up and running in the docker container by running `docker ps`. 
    * You should see a continer with the name "zaaksysteem".
    * You can also test by trying to open the Pyramid scaffold page - [http://localhost:6543/](http://localhost:6543/)

### Building from source
* You should have docker for mac installed
* To run the app locally, you just need to clone this repository and cd into the project directory.
* And then just run ``docker-compose up``. 
    * The first time you run the command, it will build the application container
    * Then it will launch the container it built
* You can verify that the API is up and running in the docker container by running `docker ps`. 
    * You should see a continer with the name "zaaksysteem".
    * You can also test by trying to open the Pyramid scaffold page - [http://localhost:6543/](http://localhost:6543/)
    
## Local development

### Pre-requisites
* Python3, pip, virtualenv, and docker should be installed on your local machine

### Actual steps
To do local development clone this repository via `git clone git@gitlab.com:tanveerg/zaaksysteem.git`. Change into this directory with `cd zaaksysteem`.
Then run the following commands:

```bash
# create and activate virtual environment
virtualenv -p python3 venv
source venv/bin/activate 
# install requirements
pip install -r requirements-dev.txt
# run tests
pytest --cov
```
Once you ensure tests run successfully, you can start developing. Happy coding!

After making your changes, you can test with a locally running app (you have two options) - 
* Locally on your machine
```bash
# make sure that tests run successfully
pytest --cov
# once you have ensured that tests run successfully, run the app locally
python setup.py develop
pserve development.ini --reload
```
* Via docker and docker-compose
* Locally on your machine
```bash
# make sure that tests run successfully
pytest --cov
# once you have ensured that tests run successfully, 
# build the container again for your changes to take effect
docker-compose build
# run the app 
docker-compose up
```

NOTE - I have used [pytest](https://docs.pytest.org/en/latest/) to run tests. 
You could use another testing framework like [nose](https://nose.readthedocs.io/en/latest/).

## Design choices / assumptions
* I divided the API into two endpoints - ``singleop`` and `complex`. The reason to do this was to have some simple testable code before I incorporated evaluation of mathematical expressions.
* I decided to use the SimpleCalculator library because I did not want to re-invent the wheel given that I could use the library to do the calculation and evaluation for expressions. 
Moreoever, the library gives a simple interface for expression evaluation which sufficed the requirements.
